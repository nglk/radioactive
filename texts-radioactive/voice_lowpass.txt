As Tina Tallon observes in 1927, the Federal Radio Commission decided to provide each station its own little 10000 hertz slice of bandwidth. So there's a segment . . . before you take the signal and modulate it into something that can be transferred . . . `{./radio-voice.sh}` in between stations known as the base band or the pre- modulated signal that had to be actually limited to 5000 hertz because amplitude modulation actually doubles the bandwidth of the signal. So initially they said, OK, we're gonna take all of our baseband signals and limit them to 5000 hertz. What that meant was all of the microphones and all of the equipment that people were using to record didn't need to go above 5000 hertz because none of that information would get transmitted.<p>

<div style="color:#A19696">**[1]TINA TALLON:. . . Newspapers and magazines repeatedly referred to women on air as “affected,” “stiff,” “forced,” and “unnatural.” . . .  they asserted that women sounded “shrill,” “nasal,” and “distorted” on the radio, and claimed that women’s higher voices created technical problems.**</div><p>


<div style="color:#A19696">**TINA TALLON: `{./lowpass.sh}`Steinberg’s experiments showed that the voiceband frequencies reduced the intelligibility of female speech by cutting out the higher frequency components necessary for the perception of certain consonants. Steinberg asserted that “nature has so designed woman’s speech that it is always most effective when it is of soft and well modulated tone.” Hinting at the age-old notion that women are too emotional, he wrote that a woman’s raised voice would exceed the limitations of the equipment, thus reducing her clarity on air**.</div> <p>

<i>[1]Extract taken from scripts, part of the performance <a href="http://w-i-t-m.net/2019/radio-active-female-monstrosity.html">"Radioactive Monstrosities"</a></i>

<ul>
<li>Tallon, T. (2019) <a href="https://www.newyorker.com/culture/cultural-comment/a-century-of-shrill-how-bias-in-technology-has-hurt-womens-voices"</a>A Century of “Shrill”: How Bias in Technology Has Hurt Women’s Voices</a>, The New Yorker.
</li>
</ul>

<img width="100%" src="../mouth.png">
