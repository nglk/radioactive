// draggable





$(document).ready(function() {
    var a = 3;
    var dragOpts = {
          cursor: "move",
          cursor: "grab",
          // axis: "y",
          distance: "0",
          // cursorAt: {
          //   top: 0,
          //   left: 0
          // } 
        };
    $('.draggable-circle').draggable(dragOpts,
    {
   start: function(event, ui) { $(this).css("z-index", a++); }
});

    $('.draggable').draggable({
   start: function(event, ui) { $(this).css("z-index", a++); }
});


  // font size relative to div draggable/resizable size
  var $wrapper = $(".scaleable-wrapper");
  $wrapper.each(function (d) {
    var $this = $(this);
    // var elHeight = $this.outerHeight();
    var elWidth = $this.outerWidth();

    $this.resizable({
      resize: doResize
    });

    function doResize(event, ui) {
      
      var scale, origin;
        
      scale = Math.min(
        // ui.size.height / elHeight,
        ui.size.width / elWidth
      );
      // scale=1.0;
      // scale=2*scale

      $this.css({
        // transform: "translate(-50%, -50%) " + "scale(" + scale + ")"
        transform: "translate(0%, 0%) " + "scale(" + scale + ")"

      });
      
    }

    var starterData = { 
        size: {
          // height: $this.height(),
          width: $this.width()
        }
    }
    // console.log("starterData", starterData, this);
    doResize(null, starterData);
 

  });
});