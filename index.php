<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="styles/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="styles/radioactive.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<header style="text-align:right;font-size:0.8em;"><a href="wac2021.html" target="_">WAC 2021 poster</a></header>
  <!-- <link href="styles/smallscreen.css" rel="stylesheet" type="text/css"> -->
<title>Radioactive Monstrosities</title>
<!-- <link rel="shortcut icon" href="mouth.png" /> -->
<style>
  h3 {font-weight: normal !important;}
 body {background:none !important;}
</style>
</head>

<body>
<!-- table of voices -->
<h1>Radio-active <div class="tooltip-wrap"><span class="hover">Monstrosities</span><div class="tooltip-content-right" style="font-size: 0.5em;" ><div>
  <?php
    include 'texts-radioactive/female_monstrosity.txt';
    ?>
</div></div></div></h1>

<!-- info about -->



<div class="draggable radioactive popup" id="popup"><span onclick="hide()" class="topleft">&times</span><div>
  <?php
    include 'texts-radioactive/about-popup.txt';
    ?>
</div></div>

<!-- <FORM ACTION="scripts/cgi-bin/echo.cgi"> -->
<!--<h3>VOICE CONTRIBUTIONS</h3>-->

<table width="100%" class="radioactive">

<tr>
    <td colspan="4">
<div class="butpopup" onclick="popup()"><i class="fa fa-file"></i>
</div>


<!-- recorder -->
      <div align="center">
        <div class="recorder">
            <input type="button" class="start"  value="record" />
            <input type="button" class="stop" value="stop" />
            <pre class="status"></pre>
        </div>

       <div id="playerContainer"></div>
       <div><button id="button_upload" onclick="upload()">upload</button></div>
       <br />
       <div id="saved_msg"></div>


       <div id="dataUrlcontainer" hidden></div>
        <pre id="log" hidden></pre>
      </div>


<div class="tooltip-wrap">
<i class="fa fa-gears hover"></i><div class="tooltip-content-right" ><div>
  <?php
    include 'texts-radioactive/instructions.txt';
    ?>
</div></div></div>

<a href="https://gitlab.com/nglk/radioactive" target="_">git</a>

<div class="tooltip-wrap"><i class="fa fa-copyright fa-flip-horizontal hover"></i><div class="tooltip-content-right" ><div>
[Angeliki Diakrousi, Radioactive Monstrosities, 2020. Rotterdam].
Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the <a href="http://artlibre.org/licence/lal/en/">Free Art License</a><br>
If you want your full name to appear in the contributors list send me your name here: angeliki@w-i-t-m.net. The voice files will be part of this platform and future performances.
</div></div></div>

    </td>
  </tr>


  <tr>
    <th><button class="moretext" id="more1" onclick="show_text('collective','more1')">+</button><div class="tooltip-wrap"><span class="hover">./collective_voice
    </span><div class="tooltip-content-right" >The speaker's voice is channeled through multiple voices and in this case through distorted mediated voices of the same person</div></div></th>
    <th><button class="moretext" id="more2" onclick="show_text('echo','more2')">+</button><div class="tooltip-wrap"><span class="hover">./echo_voice</span><div class="tooltip-content-right">A voice that is more distant from the speaker, sounding like being
in an outer space inside the medium. This voice resembles sounds from online calls, theatre stage.</div></div></th>
    <th><button class="moretext" id="more3" onclick="show_text('lowpitch','more3')">+</button><div class="tooltip-wrap"><span class="hover">./lowpitch_voice</span><div class="tooltip-content-right" >A voice that sounds more male because of lowering its pitch</div></div></th>
    <th><button class="moretext" id="more4" onclick="show_text('lowpass','more4')">+</button><div class="tooltip-wrap"><span class="hover">./lowpass_voice</span><div class="tooltip-content-right" >A voice that sounds like "shrill" if it is in high frequencies.
This script doesn't allow high frequencies to pass through</div></div></th>
  </tr>
  <tr>
    <?php
    $items=array();
    $handle=fopen('./uploads/index.jsons','r');
    if ($handle) {
      while (($line=fgets($handle)) !== false) {
        $item=json_decode($line,true);
        $items[]=$item;

      }
    }
    $items=array_reverse($items);
    echo '<td>';
    echo '<div id="collective">';
    include 'texts-radioactive/voice_collective.txt';
    echo '</div>';

    foreach($items as $item) {
      $url=substr($item['file'],3);
      if (strpos($item['type'], 'collective') !== false){
        echo '<div class=file>';
        echo "<audio id='".$item['name']."' src=".$url." preload='auto'></audio> ";
        echo "<button class='audioButton' id='but_".$item['name']."'onclick=collective_play_pause('".$item['name']."','".$url."');><i class='".$item['name']." fas fa-play'></i></button><br>";
        echo $item['name'];
        echo '</div><br />';
        }
    }
    echo '</td>';
    echo '<td>';
    echo '<div id="echo">';
    include 'texts-radioactive/voice_echo.txt';
    echo '</div>';
    foreach($items as $item) {
      $url=substr($item['file'],3);
      if (strpos($item['type'], 'echo') !== false){
        echo '<div class=file>';
                echo "<audio id='".$item['name']."' src=".$url." preload='auto'></audio> ";
        echo "<button class='audioButton' id='but_".$item['name']."'onclick=echo_play_pause('".$item['name']."','".$url."');><i class='".$item['name']." fas fa-play'></i></button><br>";
        echo $item['name'];
        echo '</div><br />';
        }
    }
    echo '</td>';
    echo '<td>';
    echo '<div id="lowpitch">';
    include 'texts-radioactive/voice_lowpitch.txt';
    echo '</div>';
    foreach($items as $item) {
      $url=substr($item['file'],3);
      if (strpos($item['type'], 'lowpitch') !== false){
        echo '<div class=file>';
                echo "<audio id='".$item['name']."' src=".$url." preload='auto'></audio> ";
        echo "<button data-js='play' class='audioButton' id='but_".$item['name']."'onclick=lowpitch_play_pause('".$item['name']."','".$url."');><i class='".$item['name']." fas fa-play'></i></button><br>";
        echo $item['name'];
        echo '</div><br />';
        }
    }
    echo '</td>';
    echo '<td>';
    echo '<div id="lowpass">';
    include 'texts-radioactive/voice_lowpass.txt';
    echo '</div>';
    foreach($items as $item) {
      $url=substr($item['file'],3);
      if (strpos($item['type'], 'lowpass') !== false){
        echo '<div class=file>';
        echo "<audio id='".$item['name']."' src=".$url." preload='auto'></audio> ";
        echo "<button data-js='play' class='audioButton' id='but_".$item['name']."'onclick=lowpass_play_pause('".$item['name']."','".$url."');><i class='".$item['name']." fas fa-play'></i></button><br>";
        echo $item['name'];
        echo '</div><br />';
        }
    }
    echo '</td>';
    ?>
  </tr>



</table>

<!-- reverb.js is from the project http://reverbjs.org/ -->
<script src="js/reverb.js"></script>
<script>
function popup() {
  popup=document.getElementById("popup");
  popup.style.top="90%";
  $("#popup").fadeIn();
}

 function hide(){
     $("#popup").hide()
    }

var context = new AudioContext();
var source = context.createBufferSource();


  function show_text(distortion,button) {
  var x = document.getElementById(distortion);
  var b = document.getElementById(button);
  if (x.style.display === "block") {
    x.style.display = "none";
    b.innerHTML = "+";
  } else {
    x.style.display = "block";
    b.innerHTML = "-";
  }
}


// 3LOWPITCH

  function lowpitch_play_pause(name,url) {

      var myIcon = document.querySelector("."+name);

      window.fetch(url)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
        .then(audioBuffer => {

        function play(){
          context.close()
           context = new AudioContext();
           source = context.createBufferSource();
           var elms = document.querySelector(".fa-pause")
           if(elms){
              elms.classList.remove('fa-pause');
              elms.classList.add('fa-play');
            }
          source.buffer = audioBuffer;
          source.playbackRate.value = 0.30;
          source.onended = onEnded;
          function onEnded() {
              isFinished = true;
              var elms = document.querySelector(".fa-pause")
              if(elms){
                elms.classList.remove('fa-pause');
                elms.classList.add('fa-play');
              }
              console.log('playback finished');
          }
          myIcon.className = name+" fas fa-pause";

          source.connect(context.destination);
          source.start();
        }

        function stop(){
          console.log(source);
          myIcon.className = name+" fas fa-play";
          source.disconnect(context.destination);
          source.stop();
        }

        if (myIcon.classList.contains('fa-pause')) {
          console.log('play')
          stop();
        }else {
          console.log('stop');
          play();
        }
       });

  }

// 2ECHO

function echo_play_pause(name,url) {
var myIcon = document.querySelector("."+name);
var masterNode;
var bypassNode;
var delayNode;
var feedbackNode;

// the delay effect is based on the delay.js script made by Dave Ramirez https://gist.github.com/ramirezd42/0432981a21abc67914f2
  window.fetch(url)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
    .then(audioBuffer => {

      function play(){
// context.close()
    context = new AudioContext();
    reverbjs.extend(context);

      // 2) Load the impulse response; upon load, connect it to the audio output.
      var reverbUrl = "./uploads/SportsCentreUniversityOfYork.m4a";
      //var reverbUrl="./uploads/StAndrewsChurch.m4a";
      var reverbNode = context.createReverbFromUrl(reverbUrl, function() {
        reverbNode.connect(context.destination);
      });
          const source = context.createBufferSource();
    source.buffer = audioBuffer;

    //create an audio node from the stream
  delayNode = context.createDelay(100)
  feedbackNode = context.createGain();
  bypassNode = context.createGain();
  masterNode = context.createGain();

  //controls
  delayNode.delayTime.value = 4.5;
  feedbackNode.gain.value = 0;
  bypassNode.gain.value = 0.5;
   source.onended = onEnded;

     function onEnded() {
              isFinished = true;
              var elms = document.querySelector(".fa-pause")
              if(elms){
                elms.classList.remove('fa-pause');
                elms.classList.add('fa-play');
              }
              console.log('playback finished');
          }

          myIcon.className = name+" fas fa-pause";
        //wire up nodes
        source.connect(reverbNode);
        reverbNode.connect(delayNode);
        delayNode.connect(feedbackNode);
        feedbackNode.connect(bypassNode);
        bypassNode.connect(masterNode);
        masterNode.connect(context.destination);
          source.start();
    }

     function stop(){
          myIcon.className = name+" fas fa-play";
          source.disconnect(context.destination);
          source.stop();
        }
        if (myIcon.classList.contains('fa-pause')) {
          console.log('play')
          stop();
        }else {
          console.log('stop');
          play();
        }
        });

}


// 1COLLECTIVE
function collective_play_pause(name,url){
   var myIcon = document.querySelector("."+name);

    window.fetch(url)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
        .then(audioBuffer => {
          function play_lowpitch(){
           source1 = context.createBufferSource();
          source1.buffer = audioBuffer;
          source1.playbackRate.value = 0.70;
          source1.onended = onEnded;
          source1.connect(context.destination);
          source1.start();
        }
        function stop_all(){
          source1.disconnect(context.destination);
          source1.stop();
          source2.stop();
          // source3.stop();
        }
        function play_lowpass(){
          var gainNode = context.createGain();
          var biquadFilter = context.createBiquadFilter();
          source2 = context.createBufferSource();
          source2.buffer = audioBuffer;
          biquadFilter.type = "lowpass";
    biquadFilter.frequency.value = 4000;
    biquadFilter.gain.value = 25;
     source2.onended = onEnded;
      // connect the nodes together
    source2.connect(biquadFilter);
    biquadFilter.connect(gainNode);
    gainNode.connect(context.destination);
    source2.start();
        }
        function play_echo(){
          var masterNode;
          var bypassNode;
          var delayNode;
          var feedbackNode;
              context = new AudioContext();
          reverbjs.extend(context);

      // 2) Load the impulse response; upon load, connect it to the audio output.
      var reverbUrl = "./uploads/SportsCentreUniversityOfYork.m4a";
      var reverbNode = context.createReverbFromUrl(reverbUrl, function() {
        reverbNode.connect(context.destination);
      });
          const source3 = context.createBufferSource();
    source3.buffer = audioBuffer;

    //create an audio node from the stream
  delayNode = context.createDelay(100)
  feedbackNode = context.createGain();
  bypassNode = context.createGain();
  masterNode = context.createGain();

  //controls
  delayNode.delayTime.value = 4.5;
  feedbackNode.gain.value = 0.1;
  bypassNode.gain.value = 1;
          source3.onended = onEnded;
//wire up nodes
        source3.connect(reverbNode);
        reverbNode.connect(delayNode);
        delayNode.connect(feedbackNode);
        feedbackNode.connect(bypassNode);
        bypassNode.connect(masterNode);
        masterNode.connect(context.destination);
          source3.start();
        }

        if (myIcon.classList.contains('fa-pause')) {
          console.log('play')
          stop_all();
        }else {
          console.log('stop');
          context.close()
           context = new AudioContext();
          var elms = document.querySelector(".fa-pause")
           if(elms){
              elms.classList.remove('fa-pause');
              elms.classList.add('fa-play');
            }
          function onEnded() {
              isFinished = true;
              // var elms = document.querySelector(".fa-pause")
              if(elms){
                elms.classList.remove('fa-pause');
                elms.classList.add('fa-play');
              }
              console.log('playback finished');
          }
          myIcon.className = name+" fas fa-pause";
           setTimeout(function ()   {
    play_lowpitch()
    }, 800);
           setTimeout(function ()   {
    play_lowpass()
    }, 1000);
    setTimeout(function ()   {
play_echo()
}, 1200);
        }
      });

}


// 4LOWPASS
function lowpass_play_pause(name,url) {
        var myIcon = document.querySelector("."+name);

  window.fetch(url)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
    .then(audioBuffer => {

    function play(){
context.close()
    context = new AudioContext();
    var gainNode = context.createGain();
    var biquadFilter = context.createBiquadFilter();
    source = context.createBufferSource();
    var elms = document.querySelector(".fa-pause")
           if(elms){
              elms.classList.remove('fa-pause');
              elms.classList.add('fa-play');
            }
    source.buffer = audioBuffer;
        // Manipulate the Biquad filter

    biquadFilter.type = "lowpass";
    biquadFilter.frequency.value = 4000;
    biquadFilter.gain.value = 25;
     source.onended = onEnded;
     function onEnded() {
              isFinished = true;
              var elms = document.querySelector(".fa-pause")
              if(elms){
                elms.classList.remove('fa-pause');
                elms.classList.add('fa-play');
              }
              console.log('playback finished');
          }

          myIcon.className = name+" fas fa-pause";

 // connect the nodes together
    source.connect(biquadFilter);
    // source.connect(context.destination);
    biquadFilter.connect(gainNode);
    gainNode.connect(context.destination);
    source.start();
     }

 function stop(){
          myIcon.className = name+" fas fa-play";
          // gainNode.disconnect(context.destination);
          source.stop();
        }

        if (myIcon.classList.contains('fa-pause')) {
          console.log('play')
          stop();
        }else {
          console.log('stop');
          play();
        }

    });
}



</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<script src="js/jquery.min.js"></script>

<!-- MP3RecorderJS https://github.com/rajivepandey/MP3RecorderJS -->
<script src="js/mp3recorder.js"></script>

<script src="js/draggable.js"></script>
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.ui.touch-punch.min.js"></script>
<script src="js/main.js"></script>
<!-- scripts for recorder -->
 <script>
    var audio_context;

    function __log(e, data) {
      log.innerHTML += "\n" + e + " " + (data || '');
    }

    $(function() {

      try {
        // webkit shim
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        navigator.getUserMedia = ( navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia ||
                         navigator.msGetUserMedia);
        window.URL = window.URL || window.webkitURL;

        var audio_context = new AudioContext;
        __log('Audio context set up.');
        __log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
      } catch (e) {
        alert('No web audio support in this browser!');
      }

      $('.recorder .start').on('click', function() {
        $this = $(this);
        $recorder = $this.parent();

        navigator.getUserMedia({audio: true}, function(stream) {
          var recorderObject = new MP3Recorder(audio_context, stream, { statusContainer: $recorder.find('.status'), statusMethod: 'replace' });
          $recorder.data('recorderObject', recorderObject);

          recorderObject.start();
        }, function(e) { });
      });

      document.getElementById("button_upload").style.display='none';

      $('.recorder .stop').on('click', function() {
        $this = $(this);
        $recorder = $this.parent();

        recorderObject = $recorder.data('recorderObject');
        recorderObject.stop();

        recorderObject.exportMP3(function(base64_data) {
          var url = 'data:audio/mp3;base64,' + base64_data;
          var au  = document.createElement('audio');

      document.getElementById("playerContainer").innerHTML = "";
      // console.log(url)
      document.getElementById("button_upload").style.display='block';

      var duc = document.getElementById("dataUrlcontainer");
      duc.innerHTML = url;

      au.controls = true;
      au.src = url;
      //$recorder.append(au);
      $('#playerContainer').append(au);


          recorderObject.logStatus('');
        });

      });

    });
    </script>


 <script>
    function upload(){
    var name = prompt('Enter your name/nickname','unnamed').replace(/ /g,"_").replace(/&/g,"and").replace(/:/g,"colon");
    var type = prompt('Enter type of distortion. Choose between collective, echo, lowpitch, lowpass').replace(/ /g,"_");
    var dataURL = document.getElementById("dataUrlcontainer").innerHTML;

      $.ajax({
      type: "POST",
      url: "scripts/uploadMp3_5.php",
      data: {
          base64: dataURL,
          name: name,
          type: type
       },
      success: function(data)
                {
                    alert (data);


                },
                error : function(data)
                {
                alert("ajax error, json: " + data);
                }

    }).done(function(o) {
      console.log('saved');
      document.getElementById("saved_msg").innerHTML = "Thanks for your contribution :<";

      });
    setInterval('location.reload()', 5000);

    }

  </script>

</body>

</html>
