This web-audio interface was made by Angeliki Diakrousi. It addresses the ways we are listening to certain female sounding voices that are perceived as inappropriate - because of the quality of their sound, their gender, the medium's distortions but also stereotypes and collective memories that they often awake. The interface allows any voice to be recorded and saved in the artist's server where the website is hosted. There they get distorted through several scripts. Then the new sounds are categorized, depending on the chosen distortion, and published back in the platform. The interface uses Web Audio API, Apache, PHP, html/css andjavascript. The different scripts are in experimental phase and the interface is built gradually. 

https://radioactive.w-i-t-m.net


## how to
- git clone https://gitlab.com/nglk/radioactive.git
- `cd radioactive`
- make a folder called uploads `mkdir uploads`
- run a server or run locally

## Web Audio Conference 2021
- artwork page in WAC: https://webaudioconf2021.com/artwork-4/
- paper: https://radioactive.w-i-t-m.net/WAC_paper_radioactive.pdf
- poster: https://radioactive.w-i-t-m.net/wac2021.html



